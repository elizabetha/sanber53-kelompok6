#Kelompok7

#Anggota

1. Muhammad Azis (https://gitlab.com/m.azis)
2. Elizabeth Gita Gracia Trimurti (https://gitlab.com/elizabeth.gita)
3. Erick Saputra Hasibuan (https://gitlab.com/erick.saputra08)

#Tema Project<br>
Forum Tanya Jawab

#ERD
<br><img src="ERD_Design.png" alt="ERD Design">

#Link Video Demo<br>
https://youtu.be/hn0eZQhDTh0

#Template <br>
AdminLTE v3.2.0 : https://github.com/ColorlibHQ/AdminLTE/releases

#Library
- Sweet Alert 2
- Laravel Breeze
- Laravel File Manager
