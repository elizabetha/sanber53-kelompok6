@extends('layout.master')

@section('judul', 'Selamat Datang')

@section('content')
<h1>CodeHelper</h1>

<p>CodeHelper adalah platform tanya jawab yang didedikasikan untuk membantu para pengembang perangkat 
    lunak dalam memecahkan masalah, memperoleh wawasan, dan berbagi pengetahuan seputar pemrograman dan pengembangan perangkat lunak. 
    Dengan CodeHelper, pengguna dapat mengajukan pertanyaan terkait dengan kode, bahasa pemrograman, framework, dan teknologi terkait lainnya,
     serta memberikan jawaban atau solusi untuk pertanyaan dari pengguna lain.</p>


@endsection

