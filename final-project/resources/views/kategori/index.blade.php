@extends('layout.master')

@section('judul', 'Pertanyaan')

@section('content')
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <a href="/kategori/create"
                class="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded inline-flex items-center">
                <svg class="w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <path d="M12 5v14M5 12h14" />
                </svg>
                Add new Category
            </a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray dark:text-gray grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
                    @forelse ($kategori as $keys => $item)
                        <div
                            class="bg-white dark:bg-gray border border-gray dark:border-gray rounded-lg shadow-md overflow-hidden">
                            <div class="p-6">
                                <h2 class="text-xl font-semibold mb-2">{{ $item->nama_kategori }}</h2>
                                <div class="flex justify-between mt-4">
                                    <a href="/kategori/{{ $item->id }}/edit"
                                        class="bg-yellow hover:bg-yellow text-white font-bold py-2 px-4 rounded">Edit</a>
                                    <form id="delete-form-{{ $item->id }}" action="/kategori/{{ $item->id }}"
                                        method="POST">
                                        @csrf
                                        @method('delete')
                                        <x-danger-button
                                            class="bg-red hover:bg-red text-white font-bold py-2 px-4 rounded"
                                            onclick="showDeleteConfirmation({{ $item->id }})">Delete</x-danger-button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="text-center col-span-full">
                            <h3 class="text-xl font-semibold">No category yet</h3>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script>
        function showDeleteConfirmation(itemId) {
            // Show confirmation dialog
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Anda tidak akan bisa mengembalikan ini!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // If confirmed, submit the form
                    document.getElementById('delete-form-' + itemId).submit();
                }
            });
        }
    </script>
@endsection