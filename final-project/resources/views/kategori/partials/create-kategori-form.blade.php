<form action="/kategori" method="POST" class="max-w-md mx-auto">
    @csrf
    <div class="mb-4">
        <label for="nama_kategori" class="block text-sm font-medium text-gray-700">Nama Kategori</label>
        <input type="text" id="nama_kategori" name="nama_kategori"
            class="mt-1 p-2 block w-full border border-gray-300 rounded-md focus:outline-none focus:border-blue-500">
        @error('nama_kategori')
            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
        @enderror
    </div>
    <div class="mb-4">
        <label for="deskripsi" class="block text-sm font-medium text-gray-700">Deskripsi</label>
        <textarea id="deskripsi" name="deskripsi" rows="4"
            class="mt-1 p-2 block w-full border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"></textarea>
        @error('deskripsi')
            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit"
        class="w-full px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600 focus:outline-none">Submit</button>
</form>
