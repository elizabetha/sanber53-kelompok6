<button {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center px-4 py-2 bg-gray dark:bg-gray border border-transparent rounded-md font-semibold text-xs text-white dark:text-gray uppercase tracking-widest hover:bg-gray dark:hover:bg-white focus:bg-gray dark:focus:bg-white active:bg-gray dark:active:bg-gray focus:outline-none focus:ring-2 focus:ring-indigo- focus:ring-offset-2 dark:focus:ring-offset-gray transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
