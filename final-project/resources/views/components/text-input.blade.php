@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'border-gray dark:border-gray dark:bg-gray dark:text-gray focus:border-indigo dark:focus:border-indigo focus:ring-indigo dark:focus:ring-indigo rounded-md shadow-sm']) !!}>
