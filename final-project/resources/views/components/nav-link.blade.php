@props(['active'])

@php
$classes = ($active ?? false)
            ? 'inline-flex items-center px-1 pt-1 border-b-2 border-indigo dark:border-indigo text-sm font-medium leading-5 text-gray dark:text-gray focus:outline-none focus:border-indigo transition duration-150 ease-in-out'
            : 'inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray dark:text-gray hover:text-gray dark:hover:text-gray hover:border-gray dark:hover:border-gray focus:outline-none focus:text-gray dark:focus:text-gray focus:border-gray dark:focus:border-gray transition duration-150 ease-in-out';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
