@props(['active'])

@php
$classes = ($active ?? false)
            ? 'block w-full pl-3 pr-4 py-2 border-l-4 border-indigo dark:border-indigo text-left text-base font-medium text-indigo dark:text-indigo bg-indigo dark:bg-indigo focus:outline-none focus:text-indigo dark:focus:text-indigo focus:bg-indigo dark:focus:bg-indigo focus:border-indigo dark:focus:border-indigo transition duration-150 ease-in-out'
            : 'block w-full pl-3 pr-4 py-2 border-l-4 border-transparent text-left text-base font-medium text-gray dark:text-gray hover:text-gray dark:hover:text-gray hover:bg-gray dark:hover:bg-gray hover:border-gray dark:hover:border-gray focus:outline-none focus:text-gray dark:focus:text-gray focus:bg-gray dark:focus:bg-gray focus:border-gray dark:focus:border-gray transition duration-150 ease-in-out';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
