<button {{ $attributes->merge(['type' => 'button', 'class' => 'inline-flex items-center px-4 py-2 bg-white dark:bg-gray border border-gray dark:border-gray rounded-md font-semibold text-xs text-gray dark:text-gray uppercase tracking-widest shadow-sm hover:bg-gray dark:hover:bg-gray focus:outline-none focus:ring-2 focus:ring-indigo focus:ring-offset-2 dark:focus:ring-offset-gray disabled:opacity-25 transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
