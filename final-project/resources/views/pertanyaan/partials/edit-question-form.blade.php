<form action="/questions/{{ $tanyabyid->id }}" method="POST" enctype="multipart/form-data" class="max-w-md mx-auto">
    @method('put')
    @csrf
    <div class="mb-4">
        <label for="judul" class="block text-sm font-medium text-gray-700">Judul</label>
        <input type="text" id="judul" name="judul"
            class="mt-1 p-2 block w-full border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
            value="{{ $tanyabyid->judul }}">
        @error('judul')
            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
        @enderror
    </div>
    <div class="mb-4">
        <label for="isi" class="block text-sm font-medium text-gray-700">Pertanyaan</label>
        <textarea id="isi" name="isi" rows="4"
            class="mt-1 p-2 block w-full border border-gray-300 rounded-md focus:outline-none focus:border-blue-500">{{ $tanyabyid->isi }}</textarea>
        @error('isi')
            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
        @enderror
    </div>
    <div class="mb-4">
        <label for="lampiran" class="block text-sm font-medium text-gray-700">Lampiran</label>
        <input type="file" id="lampiran" name="lampiran" class="mt-1 block w-full">
        @error('lampiran')
            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
        @enderror
    </div>
    <div class="mb-4">
        <label for="kategori_id" class="block text-sm font-medium text-gray-700">Kategori</label>
        <select id="kategori_id" name="kategori_id"
            class="mt-1 block w-full border border-gray-300 rounded-md focus:outline-none focus:border-blue-500">
            <option value="">-- Pilih Kategori --</option>
            @forelse ($kategori as $item)
                @if ($item->id === $tanyabyid->kategori_id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama_kategori }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endif
            @empty
                <option value="">Tidak ada Kategori</option>
            @endforelse
        </select>
        @error('kategori_id')
            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
        @enderror
    </div>
    <div class="mb-4">
        <label for="status" class="block text-sm font-medium text-gray-700">Status</label>
        <input type="text" id="status" name="status"
            class="mt-1 p-2 block w-full border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
            value="{{ $tanyabyid->status }}">
        @error('status')
            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit"
        class="w-full px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600 focus:outline-none">Submit</button>
</form>
