<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\Kategori;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class PertanyaanController extends Controller
{
    //
    public function index()
    {
        //
        $tanya = Pertanyaan::all();
        return view('pertanyaan.index', ["tanya" => $tanya]);
    }
    public function create()
    {
        //
        $kategori = Kategori::all();
        return view('pertanyaan.create', ["kategori" => $kategori]);
    }
    public function store(Request $request)
    {
        //
        $request->validate([
            'lampiran' => 'required|mimes:jpg,jpeg,png|max:2048',
            'judul' => 'required',
            'kategori_id' => 'required',
            'isi' => 'required',
            'status' => 'required'
        ]);

        $lampiranName = time() . '.' . $request->lampiran->extension();

        $request->lampiran->move(public_path('img'), $lampiranName);

        $tanya = new Pertanyaan;

        $tanya->judul = $request['judul'];
        $tanya->isi = $request['isi'];
        $tanya->kategori_id = $request['kategori_id'];
        $tanya->lampiran = $lampiranName;
        $tanya->status = $request['status'];
        $tanya->user_id = Auth::id();

        $tanya->save();

        return redirect('/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tanyabyid = Pertanyaan::find($id);
        return view('pertanyaan.detail', ["tanyabyid" => $tanyabyid]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tanyabyid = Pertanyaan::find($id);
        $kategori = Kategori::all();
        return view('pertanyaan.edit', ["tanyabyid" => $tanyabyid, "kategori" => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'lampiran' => 'mimes:jpg,jpeg,png|max:2048',
            'judul' => 'required',
            'kategori_id' => 'required',
            'isi' => 'required',
            'status' => 'required'
        ]);

        $tanyabyid = Pertanyaan::find($id);

        if ($request->has('lampiran')) {
            $path = "img/";
            File::delete($path . $tanyabyid->poster);

            $lampiranName = time() . '.' . $request->lampiran->extension();

            $request->lampiran->move(public_path('img'), $lampiranName);

            $tanyabyid->lampiran = $lampiranName;
        }

        $tanyabyid->judul = $request['judul'];
        $tanyabyid->isi = $request['isi'];
        $tanyabyid->status = $request['status'];
        $tanyabyid->kategori_id = $request['kategori_id'];

        $tanyabyid->save();

        return redirect('/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tanyabyid = Pertanyaan::find($id);
        $path = "img/";
        File::delete($path . $tanyabyid->lampiran);

        $tanyabyid->delete();

        return redirect('/questions');
    }
}
