<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class KategoriController extends Controller
{
    //
    public function index()
    {
           $kategori = Kategori::all();
           return view('kategori.index', ["kategori" => $kategori]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama_kategori' => 'required',
            'deskripsi' => 'required',
        ]);

        DB::table('kategori')->insert([
            'nama_kategori' => $request['nama_kategori'],
            'deskripsi' => $request['deskripsi'],
        ]);

        //redirect
        return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kategoribyid = DB::table('kategori')->find($id);
        return view('kategori.edit', ['kategoribyid' => $kategoribyid]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nama_kategori' => 'required',
            'deskripsi' => 'required',
        ]);

        $kategoribyid = Kategori::find($id);
        $kategoribyid->nama_kategori = $request['nama_kategori'];
        $kategoribyid->deskripsi = $request['deskripsi'];
        $kategoribyid->save();

        return redirect('/kategori')->with('status', 'Data Kategori Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $query = DB::table('kategori')->where('id', $id)->delete();
        return redirect('/kategori');
    }
}
