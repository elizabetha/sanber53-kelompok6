<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class JawabanController extends Controller
{
    public function index($id)
    {
        $jawaban = DB::table('jawaban')->where('id_pertanyaan', $id)->get();
        return view('jawaban.index', ['jawaban' => $jawaban]);
    }

    public function create($id)
    {
        return view('jawaban.create', ['id' => $id]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        DB::table('jawaban')->insert([
            'isi' => $request['isi'],
            'id_pertanyaan' => $request['id_pertanyaan'],
        ]);

        return redirect('/pertanyaan');
    }

    public function edit($id)
    {
        $jawaban = DB::table('jawaban')->where('id', $id)->first();
        return view('jawaban.edit', ['jawaban' => $jawaban]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        DB::table('jawaban')->where('id', $id)->update([
            'isi' => $request['isi'],
        ]);

        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        DB::table('jawaban')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
    

}
